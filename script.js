const button = document.querySelector("#btn-click");

button.addEventListener("click", () => {
    const section = document.querySelector("#content");
    const newParagraph = document.createElement("p");
    newParagraph.textContent = "New Paragraph";
    section.appendChild(newParagraph);
})

const section = document.querySelector("#content");
const footer = document.querySelector("footer");

if (section && footer) {
    const newButton = document.createElement("button");
    newButton.id = "btn-input-create";
    section.prepend(newButton);

    newButton.addEventListener("click", () => {
        const newInput = document.createElement("input");
        newInput.name = "userInput";
        newInput.type = "text";
        newInput.placeholder = "enter something";
        section.insertBefore(newInput, newButton.nextSibling);
    })
}



